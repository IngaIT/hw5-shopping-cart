import React from "react";
import { MainLeft } from "../mainLeft";
import { MainRight } from "../mainRight";
import styles from "../../styles/main.module.scss";

export const Main = () => {
  return (
    <div className={styles.Main__row}>
      <MainLeft />
      <MainRight />
    </div>
  );
};
